package utils;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * @author Andrei Dziubiankou
 *
 */
public class ResolveURLTag extends SimpleTagSupport {
    private String url;
    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.SimpleTagSupport#doTag()
     */
    /**
     * @param url for test url
     */
    public void setUrl(final String url) {
        this.url = url;
    }
    @Override
    public void doTag() throws JspException, IOException {
        // TODO Auto-generated method stub
        PageContext pageContext = (PageContext) getJspContext();
        pageContext.setAttribute("url", url);
        pageContext.setAttribute("resource", pageContext.getServletContext().getRealPath(url));
        getJspBody().invoke(null);
    }

}
