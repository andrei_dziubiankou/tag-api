package utils;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 * @author Andrei Dziubiankou
 *
 */
public class ListMappingTag extends BodyTagSupport {
    private static final long serialVersionUID = 1L;
    private Iterator<? extends Entry<String, ? extends ServletRegistration>> it;
    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.BodyTagSupport#doStartTag()
     */
    @Override
    public int doStartTag() throws JspException {
        // TODO Auto-generated method stub
        it = pageContext.getServletContext().getServletRegistrations().
                entrySet().iterator();
        if (it.hasNext()) {
            Map.Entry<String, ? extends ServletRegistration> rgs = it.next();
            pageContext.setAttribute("url", rgs.getKey());
            pageContext.setAttribute("servlet", rgs.getValue().getMappings());
            return EVAL_BODY_INCLUDE;
        } else {
            return SKIP_BODY;
        }
    }

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.BodyTagSupport#doAfterBody()
     */
    @Override
    public int doAfterBody() throws JspException {
        // TODO Auto-generated method stub
        if (it.hasNext()) {
            Map.Entry<String, ? extends ServletRegistration> rgs = it.next();
            pageContext.setAttribute("url", rgs.getKey());
            pageContext.setAttribute("servlet", rgs.getValue().getMappings());
            return EVAL_BODY_AGAIN;
        } else {
            return SKIP_BODY;
        }
    }

}
