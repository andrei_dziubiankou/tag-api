<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="utils" uri="http://utils.tag" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Start</title>
</head>
<body>
    <p>hello world</p>
    <a href="start.jsp">start</a><br>
    <utils:resolveurl url="images/3.jpg">
        ${url } - ${resource }<br>
    </utils:resolveurl><br>   
    <img alt="" src="images/3.jpg">
</body>
</html>