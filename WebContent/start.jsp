<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="utils" uri="http://utils.tag" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Start</title>
</head>
<body>
    <utils:listmapping>
        ${url} - ${servlet}<br>
    </utils:listmapping><br>
    <utils:resolveurl url="jsp/hello.jsp">
        ${url } - ${resource }<br>
    </utils:resolveurl><br>   
    <a href="hello">Hello</a>
    <a href="bye">bye</a>
    <a href="test">test</a>
</body>
</html>